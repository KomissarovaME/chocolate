import java.util.Scanner;

public class Chocolates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите кол-во своих финансов:");
        int money = scanner.nextInt();

        System.out.println("Введите цену за одну единицу товара:");
        int price = scanner.nextInt();

        System.out.println("Кол-во обёрток нужное для получения ещё одной шоколадки:");
        int wrap = scanner.nextInt();

        int pieces = money / price;
        int sum = 0;

        while (pieces > wrap){
            int new_choco = pieces / wrap;

            sum += sum + new_choco;
            pieces = pieces % wrap;
            pieces = pieces + new_choco;
        }
        System.out.println("Через ваши руки пройдёт "+sum+" шоколадок.");
    }
}
